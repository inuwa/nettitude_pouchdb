[![Build Status](https://img.shields.io/travis/madhums/node-express-mongoose.svg?style=flat)](https://travis-ci.org/madhums/node-express-mongoose)
[![Dependencies](https://img.shields.io/david/madhums/node-express-mongoose.svg?style=flat)](https://david-dm.org/madhums/node-express-mongoose)
[![Code Climate](https://codeclimate.com/github/madhums/node-express-mongoose/badges/gpa.svg)](https://codeclimate.com/github/madhums/node-express-mongoose)
[![Greenkeeper badge](https://badges.greenkeeper.io/madhums/node-express-mongoose.svg)](https://greenkeeper.io/)
[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/madhums/node-express-mongoose?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Gittip](https://img.shields.io/gratipay/madhums.svg?style=flat)](https://www.gratipay.com/madhums/)

## Nettitude Chart Applications
Question 1: Number of attacks per countries   (table connections will give you the IPs. You need to source from an external API the IP geolocation, IP to country. There are plenty of them free online)

Question 2: Display the 10 latest actives IPs  (table connections) (IP address and number of connections)

Question 4: Trend of attacks over 24 hours / 1 week / 1 day  (successful attacks, or unsuccessful attacks or a double graph showing successful and unsuccessful attack). The unsuccessful connections are identified by connections.connections_protocol=’pcap’

## Usage
Download The SQLite Database from : [Here](
https://nettitude.sendsafely.com/receive/?thread=EDLZ-9TVZ&packageCode=OYAuRuJUk0qK6X3sQ4dqEC6t0KNrS9pQgLy1zBuXMjA#keyCode=oV88np_cdW8JX7KFlDW2VXLzKORnHPISPIZm6n8PeCo)

    $ git clone https://inuwa@bitbucket.org/inuwa/nettitude_pouchdb.git
    $ cd nettitude
    $ cp </where/you/downloaded/dionaea.sqlite> sqlite3/
    $ mongod
    $ yarn install
    $ yarn start
