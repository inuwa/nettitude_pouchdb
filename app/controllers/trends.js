'use strict';
const Promise = require('bluebird');
const sqliteCtrl = require('./sqliteCtrl');
const database = sqliteCtrl.database;
const getResults = require('./attacksPerCountry').getResults;

var dayTrends = () => {
  let sql = 'select  remote_host, connection from connections where connection_type = \'connect\' order by connection desc limit 10';
  let dbLocation = '../sqlite3/dionaea.sqlite';
  let db = database(dbLocation);

  return getResults(sql, db);
};
var aggregateTrendStats = (successFulResults, unsuccessfulResults, firstIndexData) => {
  let aggregateData = [];
  if (firstIndexData) {
    aggregateData.push(firstIndexData);
    if (successFulResults && unsuccessfulResults) {
      for (let i = 0; i < successFulResults.length ; i++) {
        let average = (successFulResults[i].total + unsuccessfulResults[i].total) / 2;
        aggregateData.push([successFulResults[i].dates,successFulResults[i].total,unsuccessfulResults[i].total, average]);
      }
    }
  } else {
    throw new Error();
  }
  return aggregateData;
};
var weekTrends = (req, res) => {
  let sqlUnsuccessful = 'select distinct date(connection_timestamp,\'unixepoch\', \'localtime\')  as dates, count(connection_type) as total from connections where connection_protocol = \'pcap\' group by dates order by dates desc  limit 7';
  let sqlSuccessful = 'select distinct date(connection_timestamp,\'unixepoch\', \'localtime\')  as dates, count(connection_type)  as total from connections where connection_protocol != \'pcap\' group by dates order by dates desc  limit 7';
  let dbLocation = '../sqlite3/dionaea.sqlite';
  let db = database(dbLocation);
  let db2 = database(dbLocation);
  // get successful and unsuccessful attacks
  // let sucessfulAttacks = getResults(sqlSuccessful, db);
  // let unsuccessfulAttacks = getResults(sqlUnsuccessful, db);
  /* Promise.all([getResults(sqlSuccessful, db)]).spread((successfulResults) => {
    return Promise.all([getResults(sqlUnsuccessful, db)]).spread((unSuccessfulResults) => {
      if (successfulResults &&  unSuccessfulResults) {
        let firstIndexData = ['Date', 'SuccessFul', 'Unsuccessful'];
        let trendData = aggregateTrendStats(successfulResults, unSuccessfulResults, firstIndexData);
        // render the view
        return res.render('live/trends', {
          title: 'Attack Trends',
          trendData: trendData
        });
      } else {
        return res.json({ error: 'No Attack Data' });
      }
    }).catch((error) => {
      console.log(error);
    });
  }).catch((error) => {
    console.log(error);
  }); */
  Promise.all([getResults(sqlSuccessful, db), getResults(sqlUnsuccessful, db2)]).spread((successfulResults, unSuccessfulResults) => {
    console.log('successfulResults: %s, unSuccessfulResults: %s', JSON.stringify(successfulResults), JSON.stringify(unSuccessfulResults));
    if (successfulResults &&  unSuccessfulResults) {
      let firstIndexData = ['Date', 'SuccessFul', 'Unsuccessful', 'Average'];
      let trendData = aggregateTrendStats(successfulResults, unSuccessfulResults, firstIndexData);
      // render the view
      res.render('live/trends', {
        title: 'Attack Trends',
        trendData: trendData
      });
    } else {
      res.json({ error: 'No Attack Data' });
    }
  }).catch((error) => {
    console.log(error);
  });
};
exports.weekTrends = weekTrends;