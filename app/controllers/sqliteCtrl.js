
// get the data from the sqlite database

const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const Promise = require('bluebird');
/**
 * Instantiate the Database
 * @param {string} dbLocation 
 */
var database = (dbLocation) => {
  return new sqlite3.Database(path.join(__dirname , dbLocation));
};

/**
 * Run query from a string
 * @param {database instance} db 
 * @returns {promise} rows
 */
var queryResults = (db, sql) => {
  return new Promise((_resolve, _reject) => {
    db.all(sql, [], (err, rows) => {
      if (err) {
        _reject(err);
      }
      /* rows.forEach((row) => {
      }); */
      _resolve(rows);
    });
  });
};
/**
 * Close the database connection
 * @param {database instance} db 
 */
var closeDatabaseConnection = (db) => {
  return db.close();
};

module.exports = {
  database: database,
  queryResults: queryResults,
  closeDatabaseConnection: closeDatabaseConnection
};