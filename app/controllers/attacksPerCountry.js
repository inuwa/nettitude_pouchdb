'use strict';
const Promise = require('bluebird');
const sqliteCtrl = require('./sqliteCtrl');
const database = sqliteCtrl.database;
const queryResults = sqliteCtrl.queryResults;
const closeDatabaseConnection = sqliteCtrl.closeDatabaseConnection;
const geoip = require('geoip-lite');
const allCountries = require('all-countries');
const _ = require('lodash');

/**
 * Returns the results of an SQL Query
 * from an SQLite Instance
 * @param {string} sql 
 * @param {database} db 
 */
var getResults = (sql, db) => {
  return Promise.all([queryResults(db, sql)]).spread((rowResults) => {
    closeDatabaseConnection(db);
    return rowResults;
  }).catch((error) => {
    console.log(error);
  });
};
/**
 * Returns all the countries in the world 
 * with in ISO2 format. Each index represented by
 * an object e.g [{country: 'US', total: 0 }...{},{}]
 */
var allCountryData = () => {
  let allCountriesArray = allCountries.getCountryCodes;
  let allCountryData = allCountriesArray.map(country => {
    let ctryData = {};
    ctryData.country = country;
    ctryData.total = 0;
    return ctryData;
  });
  return allCountryData;
};
/**
 * Gets SQl query results and converts them to an array 
 * for the graph
 * @param {array} rowResults 
 * @return {array} aggregateData
 */
var aggregateCountryStats = (rowResults) => {
  // Gets all countries and attack total
  let allCtryData = allCountryData();
  // Adds up all the totals and returns attacks per country
  rowResults.map(rowResult => {
    // Get the ISO2 Name of the country from the database
    if (rowResult.remote_host !== '') {
      // lookup country location from ip address
      let geo = geoip.lookup(rowResult.remote_host);
      if (geo) {
        let countryName = geo.country;
        // Get the total number of attacks
        let countryTotal = rowResult.total;
        // find the country that matches from all the stored countries
        let countryIndex = _.findIndex(allCtryData, (ctryData) => { 
          return ctryData.country === countryName; 
        });
        // If there is a match, push the data to the array to be used by the chart app
        if (countryIndex !== -1) {
          // create a temporary object
          let tempData = {};
          tempData.country = countryName;
          // Add the total to the matched index
          tempData.total = allCtryData[countryIndex].total + countryTotal;
          // Replace the new total with the old total
          allCtryData.splice(countryIndex, 1, tempData);
        } 
      }
    }
  });
  return allCtryData;
};
/**
 * Returns an array formated for Google Chart
 * @param {array} aggregateData 
 * @param {*} firstIndexData 
 * @returns array chartIndexValuechartArrya
 */
var chartArrayFormat = (aggregateData, firstIndexData) => {
  let chartArray = [firstIndexData];
  aggregateData.map((element) => {
    chartArray.push([element.country, element.total]); 
  });
  return chartArray;
};
/**
 * GET /percountry
 * Runs SQL and then displays the view.
 * @param {*} req 
 * @param {*} res 
 */
var showAggregateData = (req, res) => {
  let sql = 'select remote_host, count(*) as total from connections group by remote_host';
  let dbLocation = '../sqlite3/dionaea.sqlite';
  let db = database(dbLocation);
  Promise.all([getResults(sql,db)]).spread((rowResults) => {
    if (rowResults) {
      // Collate Data into Countries
      let aggregateData = aggregateCountryStats(rowResults);
      let firstIndexData = ['Country', 'Attacks'];
      // Present on format to display for the chart
      let chartArrayData = chartArrayFormat(aggregateData, firstIndexData);
      return chartArrayData;
    } else {
      res.json({ error: 'No Attack Data' });
    }
  }).then((chartArrayData) => {
    res.render('live/attacksPerCountry', {
      title: 'Attacks Per Country',
      chartArrayData: chartArrayData
    });
  }).catch((error) => {
    console.log('error: ', error);
  });
};
exports.showAggregateData = showAggregateData;
exports.getResults = getResults;