'use strict';
const Promise = require('bluebird');
const sqliteCtrl = require('./sqliteCtrl');
const database = sqliteCtrl.database;
const closeDatabaseConnection = sqliteCtrl.closeDatabaseConnection;
const geoip = require('geoip-lite');
const allCountries = require('all-countries');
const _ = require('lodash');
const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-find'));

/**
 * GET /perctry
 * Checks that PouchDB is setup, syncs with SQLite 
 * and Client Browser then gets chart data for Pouchdb 
 * on the client side 
 * @param {*} req 
 * @param {*} res 
 */
var showPouchData = (req, res) => {
  const pouchName = 'attackspercountry';
  const pouch = createPouch(pouchName);
  const view = 'live/attacksPerCountryPouch';
  pouch.info().then((details) => {
    // Check that the database exists and no documents
    // have been added
    if ( details.doc_count === 0 && details.update_seq === 0 ) {
      // PouchDB Database does not exist
      // Create on from results of SQLite Query
      const db = database('../sqlite3/dionaea.sqlite');
      const sql = 'select remote_host, count(*) as total from connections group by remote_host';
      return Promise.all([getResults(sql,db)]).spread((rowResults) => {
        if (rowResults) {   
          // insertIntoPouch
          return Promise.map(rowResults, (rowResult) => {
            return pushToPouch(pouch, rowResult);
          });   
        } else {
          return res.json({ error: 'No Attack Data' });
        }
      }).then((promiseData) => {
        return res.render(view, {
          title: 'Attacks Per Country',
        });
      }).catch((error) => {
        console.log('error: ', error);
      });
    }
    else {
      // If PouchDb Database exists
      return res.render(view, {
        title: 'Attacks Per Country',
      });
    }
  }).catch((error) => {
    console.log('error: ' + error);
  });
};
/**
 * Creates a pouchdb instance
 * @param {String} dbName 
 * @returns {Object Database} pouch
 */
var createPouch = (dbName) => {
  let pouch = new PouchDB(dbName);
  return pouch;
}; 
/**
 * Pushes data to pouchdb instance
 * @param {Object Database} pouch 
 * @param {Array} doc 
 */
var pushToPouch = (pouch, doc) => {
  var addToPouch = () => {
    if (doc[0] && doc[1]) {
      if (doc[0] !== undefined && doc[1] !== undefined) {
        pouch.put({
          _id: doc[0],
          title: 'Attacks Per Country',
          total: doc[1],
          country: doc[0]
        }).then((response) => {
          console.log('Added: ', response);
        }).catch((error) => {
          console.log('Error: ', error);
        });
      }
    }
  };
  // Add documents to pouch
  return Promise.all([addToPouch()]).spread((documents) => {
  }).catch((error) => {
    console.log('Error: ', error);
  });
};
/**
 * Runs an SQLite Queru and then returns array data
 * @param {SQLite database} db 
 * @param {String} sql 
 */
var queryResults = (db, sql) => {
  return new Promise((_resolve, _reject) => {
    let allCtryData = allCountryData();
    db.each(sql, [], (err, row) => {
      if (err) {
        _reject(err);
      } 
      let stats = aggregateStats(row, allCtryData);
      allCtryData.splice(stats.countryIndex, 1, [stats.countryName, stats.tempTotal]);
    }, (error, context) => {
      if (error) {
        _reject(error);
      }
      else {
        _resolve(allCtryData);
      }
    });
  });
};
/**
 * Returns the total stats for a defined iP
 * @param {Object} rowResult 
 * @param {Array} allCtryData 
 * @returns {JSON Object} stats
 */
var aggregateStats = (rowResult, allCtryData) => {
  let stats = {};
  if (rowResult){
    if (rowResult.remote_host && rowResult.remote_host !== '') {
      // Get the Loaction details of the Attack ip
      let geo = geoip.lookup(rowResult.remote_host);
      if (geo) {
        // Get the countryName
        let countryName = geo.country;
        let countryTotal = rowResult.total;
        let countryIndex = _.findIndex(allCtryData, (ctryData) => { 
          return ctryData[0] === countryName; 
        });
        // If there is data on the ipAddress
        if (countryIndex != -1 ) {
          let tempTotal = allCtryData[countryIndex][1] + countryTotal;
          // Return the total number of attacks for that Country
          stats = { 
            countryName:countryName, 
            tempTotal:tempTotal , 
            countryIndex: countryIndex
          };
        } 
      } 
    }
  }
  return stats;
};
/**
 * Returns the results of an SQL Query
 * from an SQLite Instance
 * @param {string} sql 
 * @param {database} db 
 */
var getResults = (sql, db) => {
  return Promise.all([queryResults(db, sql)]).spread((rowResults) => {
    closeDatabaseConnection(db);
    return rowResults;
  }).catch((error) => {
    console.log(error);
  });
};
/**
 * Returns all the countries in the world 
 * with in ISO2 format. Each index represented by
 * an object e.g [{country: 'US', total: 0 }...{},{}]
 */
var allCountryData = () => {
  let allCountriesArray = allCountries.getCountryCodes;
  let allCountryData = [];
  allCountryData = allCountriesArray.map(country => {
    // return country name and total count. Count is initially zero
    return [country, 0];
  });
  return allCountryData;
};

exports.showPouchData = showPouchData;
exports.getResults = getResults;