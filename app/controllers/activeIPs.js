'use strict';
const Promise = require('bluebird');
const sqliteCtrl = require('./sqliteCtrl');
const database = sqliteCtrl.database;
const getResults = require('./attacksPerCountry').getResults;

/**
 * Aggregate IP Stats
 * @param {Array} rowResults 
 * @param {Array} firstIndexData 
 */
var aggregateIpStats = (rowResults, firstIndexData) => {
  let aggregateData = [firstIndexData];
  rowResults.map(rowResult => {
    if ( rowResult.remote_host !== '' && rowResult.connection) {
      aggregateData.push([rowResult.remote_host, rowResult.connection]);
    } 
  });
  return aggregateData;
};
/**
 * GET /activeips 
 * Gets the last active ips
 * @param {*} req 
 * @param {*} res 
 */
var latestActiveIPs = (req, res) => {
  let sql = 'select  remote_host, connection from connections where connection_type = \'connect\' order by connection desc limit 10';
  let dbLocation = '../sqlite3/dionaea.sqlite';
  let db = database(dbLocation);
  return Promise.all([getResults(sql, db)]).spread((rowResults) => {
    if (rowResults) {
      let firstIndexData = ['IPs', 'Connection'];
      let aggregateData = aggregateIpStats(rowResults, firstIndexData);
      
      // render the view
      res.render('live/activeIps', {
        title: 'Active IPs',
        aggregateData: aggregateData
      });
    } else {
      res.json({ error: 'No Attack Data' });
    }
  }).catch(error => {
    console.log(error);
  });
};
exports.latestActiveIPs = latestActiveIPs;