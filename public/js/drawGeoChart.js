google.charts.load('current', {
  'packages':['geochart'],
  // Note: you will need to get a mapsApiKey for your project.
  // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
  'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
  });
  google.charts.setOnLoadCallback(drawRegionsMap);
  function drawRegionsMap() {
  var data = google.visualization.arrayToDataTable(chartArrayData);
  var options = {
    colorAxis: {
      // colors: ['#00853f', 'black', '#e31b23']
      colors: ['#FFA500', '#A52A2A','#FF0000', 'black']
    },
    backgroundColor: '#81d4fa',
    datalessRegionColor: '#f8bbd0',
    defaultColor: '#f5f5f5',
    legend: { textStyle: { color: 'blue', fontSize: 16 } },
    magnifyingGlass: { enable: true, zoomFactor: 7.5 }
  };
  var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
  chart.draw(data, options);
}