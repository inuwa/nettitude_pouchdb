/* var syncError = () => {
  syncDom.setAttribute('data-sync-state', 'error');
}; */
var showData = () => {
  var localDB = new PouchDB('localPouch');
  var remoteDB = new PouchDB('http://localhost:3000/db/attackspercountry');
  // syncDom.setAttribute('data-sync-state', 'syncing');
  localDB.replicate.from(remoteDB);
  localDB.allDocs({ include_docs: true }).then((doc) => {
    let pouchArrayData = newFunction();
    console.log('doc.rows: ', Array.isArray(pouchArrayData));
    let docRows = doc.rows;
    pouchArrayData = docRows.map((row) => {
      return [row.doc.country, row.doc.total];
    });
    pouchArrayData.unshift(['Country', 'Attacks']);
    return pouchArrayData;
  }).then((pouchArrayData) => {
    google.charts.load('current', {
      'packages':['geochart'],
      // Note: you will need to get a mapsApiKey for your project.
      // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
      'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
      });
      google.charts.setOnLoadCallback(drawRegionsMap);
      function drawRegionsMap() {
      var data = google.visualization.arrayToDataTable(pouchArrayData);
      var options = {
        colorAxis: {
          // colors: ['#00853f', 'black', '#e31b23']
          colors: ['#FFA500', '#A52A2A','#FF0000', 'black']
        },
        backgroundColor: '#81d4fa',
        datalessRegionColor: '#f8bbd0',
        defaultColor: '#f5f5f5',
        legend: { textStyle: { color: 'blue', fontSize: 16 } },
        magnifyingGlass: { enable: true, zoomFactor: 7.5 }
      };
      var chart = new google.visualization.GeoChart(document.getElementById('sync-wrapper'));
      chart.draw(data, options);
    }
  }).catch((error) => {
    console.log(error);
  });
};
showData();
function newFunction () {
  let pouchArrayData = [];
  return pouchArrayData;
}

