google.charts.load('current', { 'packages':['corechart'] });
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable(trendData);

    var options = {
      title : 'Attacks Over Last 7 Days',
      vAxis: { title: 'Total' },
      hAxis: { 
        title: 'Dates',
        slantedText: 'true'
      },
      seriesType: 'bars',
      series: { 2: { type: 'line' } }
    };

    var chart = new google.visualization.ComboChart(document.getElementById('trend_div'));
    chart.draw(data, options);
  }