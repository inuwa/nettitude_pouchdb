google.charts.load('current', { packages: ['corechart', 'bar'] });
google.charts.setOnLoadCallback(drawBasic);

function drawBasic () {

      var data = google.visualization.arrayToDataTable(aggregateData);

      var options = {
        title: 'Latest Active IPs',
        chartArea: { width: '50%' },
        hAxis: {
          title: 'Connection',
          minValue: 395000,
          maxValue: 400000
        },
        vAxis: {
          title: 'IPs'
        },
        explorer: {
          maxZoomOut:10,
          keepInBounds: true
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }